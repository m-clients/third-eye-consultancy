<style>
#header {
    background: #090422db;
}
</style>
<?php require_once('partials/header.php');?>


<main id="main">
    <section style="background-color: #F0F2F5;">
        <div class="container py-5 mt-5" style="">
            <br>
            <h2 class="title text-center">Referal statememt</h2>
            <div class="border"></div>
            <br>
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card">
                        <div class="card-body">
                            <p>
                                Thank you for being our customer. We truly value our professional relationship.
                            </p>
                            <p>
                                We noticed that we are receiving a lot of referrals from our existing clients and we
                                want to
                                thank you and give you back to the best of our ability.

                                From now on if you refer us and the referral converts, we will transfer $200 into
                                your
                                PayPal account for each such referral.

                            </p>
                            Please feel free to reach out in case of any questions or concerns.
                            </p>
                            <p>
                                Thank you
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
<?php require_once('partials/footer.php');?>