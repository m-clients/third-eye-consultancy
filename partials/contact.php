    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title">Contact Us</h2>
                    <div class="border"></div>
                    <p>Please note: Our current average response time is less than an hour</p>
                    <div class="social-links">
                        <div class='item'>
                            <img src="./public/img/email.svg" alt="">
                            <a href="mailto:sam@thirdeyeaccounting.com">
                                sam@thirdeyeaccounting.com
                            </a>
                        </div>
                        <div class="item">
                            <img src="./public/img/web.svg" alt="">
                            <a href="https://thirdeyeaccounting.com">
                                www.thirdeyeaccounting.com
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <br><br>
                    <div class="address-container">
                        <h5>US OFFICE</h5>
                        <!-- <address>800 Riverside Dr. Apt 7C, <br>  New York, NY 10032</address> -->
                        <address>1317 Edgewater Dr #5751 <br> Orlando, FL 32804</address>
                        <br>
                        <h5>INDIA OFFICE</h5>
                        <address>Sector 15A, Faridabad,<br> Haryana, India, 121007</address>
                    </div>
                </div>
                <div class="col-lg-6">
                    <br><br>
                    <div class="form">
                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                        <div id="errormessage">Internal server error. please try again</div>
                        <form role="form" class="contactForm" method='post'>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Your Name" data-rule="minlen:4"
                                            data-msg="Please enter at least 4 chars" />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" id="email"
                                            placeholder="Your Email" data-rule="email"
                                            data-msg="Please enter a valid email" />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="tel" class="form-control" name="phone" id="phone"
                                            placeholder="Your Phone" data-rule="required"
                                            data-msg="Please enter your phone number" />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="business" id="business"
                                            placeholder="Name of Your Business" data-rule="required"
                                            data-msg="Please enter your business" />
                                        <div class="validation"></div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" rows="5" data-rule="required"
                                            data-msg="Please write something for us" placeholder="Message"></textarea>
                                        <div class="validation"></div>
                                    </div>
                                    <div class="text-center"><button type="submit" id="submitBtn">Send Message</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <br>
                    <p>This site is protected by reCAPTCHA and the Google Privacy Policy and Terms of Service apply.</p>
                </div>

            </div>

        </div>
    </section><!-- #contact -->