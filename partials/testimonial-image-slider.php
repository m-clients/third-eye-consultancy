<?php

$imgGallery = array(
    array(
        "title"     => "",
        'video_id' => 'yukIyAkkzVA',
        'local_image_path' => 'https://i1.ytimg.com/vi/yukIyAkkzVA/mqdefault.jpg',
    ),
    array(
        "title"     => "Jerry",
        'video_id' => '_cNESlKA4Fo',
        'local_image_path' => './public/img/testimonials/1.PNG',
    ),
    array(
        "title" => "Dominic",
        'video_id' => 'exSe0x-D-wU',
        'local_image_path' => './public/img/testimonials/2.PNG',
    ),
    array(
        "title" => "Brooke",
        'video_id' => 'E4PgIDlrHcM',
        'local_image_path' => './public/img/testimonials/3.PNG',
    ),
    array(
        "title"     => "Jay",
        'video_id' => 'S5OmKWNUplA',
        'local_image_path' => './public/img/testimonials/4.PNG',
    ),
    array(
        "title" => "Juliana",
        'video_id' => 'mtF663JUEZo',
        'local_image_path' => './public/img/testimonials/5.PNG',
    ),
    array(
        "title"    => "Darrel",
        'video_id' => 'QgWi5_0H_G0',
        'local_image_path' => './public/img/testimonials/6.PNG',
    ),
    array(
        "title"    => "Mindy",
        'video_id' => 'ME2U_1o4rT0',
        'local_image_path' => './public/img/testimonials/7.PNG',
    ),
    array(
        "title"    => "Hayden",
        'video_id' => 'J-LgVOuoSF8',
        'local_image_path' => './public/img/testimonials/7.PNG',
    ),
    array(
        "title"    => "Jay",
        'video_id' => 'Wsb0rnj4QuE',
        'local_image_path' => './public/img/testimonials/7.PNG',
    ),
);

?>


<div style="display:none;" class="html5gallery" data-skin="gallery" data-width="480" data-height="200"
    data-thumbwidth="200" data-thumbheight="100">
    <!-- Add Youtube video to Gallery -->
    <?php foreach($imgGallery as $item):?>
    <a href="http://www.youtube.com/embed/<?=$item['video_id']?>" class="html5gallerylightbox">
        <img src="https://img.youtube.com/vi/<?=$item['video_id']?>/maxresdefault.jpg" alt="<?=$item['title']?>">
    </a>
    <?php endforeach;?>
</div>