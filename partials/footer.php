<!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; 2021 Copyright <strong> <a href="/">ThirdEyeAccounting.com</a> </strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed and Developed By <a href="http://wayfance.com/" target="_blank">WayFance.com</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#hero" class="back-to-top">
  &#10094;
  </a>
  
  <!-- <div id="stickLayer" style="display:none;" class="stick_popup">
  <div class="over-layer"></div>
		<div class="stick_close" onclick="$.stick_close()">&times;</div>
		<div class="stick_content text-center">
            <h2>Want to discuss your requirements with a human?</h2>
            <a href="https://wa.me/9614966299" class="btn btn-whatsapp" target="_blank">
            <img src="./public/img/whatsapp.png" alt=""> Start a whatsapp conversation
          </a>
		</div>
	</div> -->

  
  <script src="./public/js/jquery.min.js" ></script>
  <script src="./public/build/js/app.js?ver=<?=$appVersion?>" ></script>
  <!-- <script src="./public/js/jquery.blueimp-gallery.min.js" defer></script> -->
  <script src="./public/js/lib/html5gallery.js"></script>
  <script src="./public/contactform/contactform.js?ver=<?=$appVersion?>"></script>
  
  
  <div id="exitEntendModal" class="modal fade2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="stick_content text-center">
                  <h2>Want to discuss your requirements with a human?</h2>
                  <a href="https://wa.me/+919540166314" class="btn btn-whatsapp" target="_blank">
                  <img src="./public/img/whatsapp.png" alt=""> Start a whatsapp conversation
                </a>
          </div>
          <div class="stick_close" onclick="$.stick_close()">&times;</div>
          <div class="over-layer"></div>
        </div>
      </div>
    </div>
  </div>
<script>
//     $(document).ready(function() {
//     $.stickToMe({
//         layer: '#exitEntendModal',
//         cookie: true,
//         maxamount: 1,
//         bgclickclose: false,
//         cookieExpiration: 1 * 24 * 60 * 60 * 1000
//     });
// });
  </script>
</body>
</html>