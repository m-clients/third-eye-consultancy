<?php 
  $appVersion = "2.0.4";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Third Eye Accounting</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="./public/img/third-eye-fav.png" rel="icon">
    <link href="./public/img/apple-touch-icon.png" rel="apple-touch-icon">

    <link href="./public/css/bootstrap.min.css" rel="stylesheet">
    <link href="./public/build/css/app.css?ver=<?=$appVersion?>" rel="stylesheet">
    <!-- <link href="./public/build/css/gallery.css" rel="stylesheet"> -->


</head>

<body>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-205491822-1">
    </script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-205491822-1');
    </script>

    <!--==========================
  Header
  ============================-->
    <header id="header">
        <div class="container">

            <div id="logo" class="pull-left">
                <a href="/">Third Eye Accounting</a>
                <span class="logo-text">More than just the Debits & Credits</span>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="#hero" class="scrollable">Home</a></li>
                    <li><a href="#testimonials" class="scrollable">Testimonials</a></li>
                    <li><a href="#message" class="scrollable">Sam's Message</a></li>
                    <li><a href="#services" class="scrollable">Services</a></li>
                    <li><a href="#pricing" class="scrollable">Pricing</a></li>
                    <li><a href="#contact" class="scrollable">Contact Us</a></li>
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </header><!-- #header -->