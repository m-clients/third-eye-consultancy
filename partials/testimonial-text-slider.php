
<!-- text slider -->
<?php 
	$textContents = array(
		array(
			"content" => "Sam was extremely helpful, even when I was unsure of what I wanted. If you are looking for an organized book keeper, Sam Is definitely the go-to. Very genuine man, and fun to talk to. I will be using him for all my stores.",
			"by"	  => "<b>Noah Davis</b> Atlanta, Georgia"
		),
		array(
			"content" => "It was great experience. He know what he's doing.",
			"by"	  => "<b>Gary Martin,</b> Atlanta, Georgia"
		),
		array(
			"content" => "Sam is very detailed and goes above and beyond.",
			"by"	  => "<b>Juliana Soleil,</b> Oviedo, Florida"
		),
		array(
			"content" => "Very reliable and great at communicating with you.",
			"by"	  => "<b>Madison Hummel,</b> Washington, DC"
		),
		array(
			"content" => "Great work!",
			"by"	  => "<b>Jon Webb,</b> New Orleans, Texas"
		),
		array(
			"content" => "Great Service !",
			"by"	  => "<b>R S Bath,</b> Yorba Linda, California"
		),
		array(
			"content" => "I was extremely happy with the service that Sam provided. I will be using him in the future and happy to recommend him to any of my colleagues.",
			"by"	  => "<b>M Kenny,</b> Humbel, Texas"
		)
		);
?>


<section class="customer-revs">
  <!-- Slideshow container -->
  <div class="slideshow-container">
  <?php foreach($textContents as $item):?>
    <div class="mySlides">
		<q><?=$item['content']?></q>
      	<p class="author"><?=$item['by']?></p>
    </div>
<?php endforeach;?>

    <!-- Next/prev buttons -->
    <div class="controls">
    <a class="prev-quote"></a>
    <a class="next-quote"></a>
    </div>
  </div><!-- END slidehow-container -->
</section>


<style>

.slideshow-container {
  position: relative;
  margin: 0 auto;
  text-align: center;
  min-height: 10rem;
  padding: 2rem 2.5rem 2rem 3.5rem;
  background: rgb(255 255 255 / 27%);
}

/* Slides */
.mySlides {
	display: none;
}
.mySlides.active{
	transition: all 3s ease-in;
}
.mySlides q{
	font-size: 20px;
	margin-bottom: 16px;
}
.mySlides p{
    margin: 0;
    font-size: 18px;
}
.controls{
	margin-top: 15px;
}
/* next & previous buttons */
.prev-quote,
.next-quote {
	height: 40px;
    width: 40px;
    background-image: url(./public/img/arrow.png);
    background-size: 123%;
    border: 1px solid #2e245d;
    position: relative;
    background-repeat: no-repeat;
    background-position: 12px -3px;
    display: inline-block;
    cursor: pointer;
}
.next-quote {
	background-position: -19px -3px;
}

/* on hover, add a black bg color with a little bit see-through */
.prev-quote:hover,
.next-quote:hover {
  opacity: 0.6;
  text-decoration: none;
}

@media (max-width: 768px) {
	.mySlides q{
		font-size: 16px;
	}
	.mySlides p{
    	font-size: 14px;
	}
	.slideshow-container{
		padding-right: 10px;
    	padding-left: 10px;
	}
} 

</style>
