 <!--==========================
    Hero Section
  ============================-->
  <section id="hero">
    <div class="hero-container">
      <div class="bb">
      </div>
      <!-- <img src="./public/img/logo-eye.png" alt=""> -->
      <h1>Stress-Free Accounting & Bookkeeping Solutions <br> For Small & Medium Business.</h1>
    </div>
    <img class="bar" src="./public/img/bar.svg" alt="">
  </section><!-- #hero -->