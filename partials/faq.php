 <!--==========================
      testimonials Us Section
    ============================-->
    <section id="faq">
      <div class="container">
        <div class="row testimonial-container">
          <div class="col-lg-12 content">
            <h2 class="title">Frequently Asked Questions</h2>
            <div class="border"></div>
            <br>
            <div id="accordion">
              <!-- One -->
              <div class="card">
                <div class="card-header f-header active" id='one'>
                Why should we trust you/ choose you over the others?
                </div>
                <div  class="collapse show-content">
                  <div class="card-body">
                  You can trust us as we are a team of CPAs from the US, ACCA from the UK and CA from India. Our team members have atleast 6 years of experience in the various fields of accounting and finance such as small & medium business accounting, credit risk analysis, internal and external auditing etc. We are certified quickbooks pro advisors as well as quickbooks advance pro advisors. We will bring to the table more than just the debits and credits.
                  </div>
                </div>
              </div>
              <!-- Two -->
              <div class="card">
                <div class="card-header f-header" id="two">
                How do you ensure data security?
                </div>
                <div class="collapse">
                  <div class="card-body">
                  <p>Any data shared will be kept confidential and will not be shared with any 3rd party under any circumstances. We are open to any kind of NDA for the same.</p>
                  <p>We follow GDPR compliant practices, use VPN and encryption to safeguard clientele data.<br>
            Also, being members of the largest professional organizations, we are legally bound to enforce highest standards of data privacy.</p>
                  </div>
                </div>
              </div>
              <!-- Three -->
              <div class="card">
                <div class="card-header f-header" id="three">
                We want someone to visit our premises for conducting the job. Are you available for the same?
                </div>
                <div class="collapse">
                  <div class="card-body">
                  <p>After the COVID outbreak we have amended our policies. We now work fully remotely and are a successful virtual firm with no physical contacts.<br>
            Unfortunately, we won’t be able to physically visit your location.</p>
                  </div>
                </div>
              </div>
              <!-- Four -->
              <div class="card">
                <div class="card-header f-header" id="four">
                What is your clientele, how many monthly clients do you work with?
                </div>
                <div class="collapse">
                  <div class="card-body">
                  <p>As of now we are fortunate to be working with clients spread all across the United states. We presently serve more than 150 clients on monthly basis.<br>
                  Apart from that we are blessed to be associated with even more yearly clients and some CPA firms as our B2B clients as well.</p>
                  </div>
                </div>
              </div>
              <!-- Five -->
              <div class="card">
                <div class="card-header f-header" id="five">
                We need accounting for many previous months and perhaps years as well. Can you offer some concessions on the price?
                </div>
                <div  class="collapse">
                  <div class="card-body">
                  <p>Sure, since you have many months to be worked upon, we will be happy to offer some exciting discounts. Please contact us for more information.</p>
                  </div>
                </div>
              </div>
              <!-- Six -->
              <div class="card">
                <div class="card-header f-header" id="six">
                My business is hit by Covid 19? Can i expect a concession for the services?
                </div>
                <div  class="collapse">
                  <div class="card-body">
                  <p>Due to an unforeseen circumstance many businesses around the world have been affected by the Covid 19 pandemic. We understand the complication it has caused to your business. But, no worries, if you would like to talk to us and get a customized quote we are open to hear you and help.</p>
                  </div>
                </div>
              </div>
              <!-- //END -->
            </div>
          </div>
        </div>
      </div>
    </section><!-- #faq -->