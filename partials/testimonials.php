 <!--==========================
      testimonials Us Section
    ============================-->
    <section id="testimonials">
      <div class="container">
        <div class="row testimonial-container">
          <div class="col-lg-12 content order-lg-1 order-2">
            <h2 class="title">Client's  Testimonials/Why Choose Us?</h2>
            <div class="border"></div>
            <p> No task is too small for us and no task is too big for us.</p>

            <?php require_once('partials/testimonial-image-slider.php');?>
           
            <br><br>
            <h2 class="title">Some More Testimonials</h2>
            <div class="border"></div>
            <br>
            <?php require_once('partials/testimonial-text-slider.php');?>

          </div>
        </div>
      </div>
    </section><!-- #about -->



    <style>
.blueimp-gallery > .slides > .slide > .video-content > video,
.blueimp-gallery > .slides > .slide > .video-content > iframe,
.blueimp-gallery > .slides > .slide > .video-content > .video-cover {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: none;
}
.blueimp-gallery > .slides > .slide > .video-content > .video-cover {
  background: center no-repeat;
  background-size: contain;
}
.blueimp-gallery > .slides > .slide > .video-iframe > .video-cover {
  background-color: #000;
  background-color: rgba(0, 0, 0, 0.7);
}
.blueimp-gallery > .slides > .slide > .video-content > .video-play {
  position: absolute;
  top: 50%;
  right: 0;
  left: 0;
  margin: -64px auto 0;
  width: 128px;
  height: 128px;
  background: url('./public/img/video-play.png') center no-repeat;
  opacity: 0.8;
  cursor: pointer;
}
.blueimp-gallery-svgasimg > .slides > .slide > .video-content > .video-play {
  background-image: url('./public/img/video-play.svg');
}
.blueimp-gallery > .slides > .slide > .video-playing > .video-play,
.blueimp-gallery > .slides > .slide > .video-playing > .video-cover {
  display: none;
}
.blueimp-gallery > .slides > .slide > .video-loading > .video-play {
  background: url('./public/img/loading.gif') center no-repeat;
  background-size: 64px 64px;
}
.blueimp-gallery-smil > .slides > .slide > .video-loading > .video-play {
  background-image: url('./public/img/loading.svg');
}

/* IE7 fixes */
* + html .blueimp-gallery > .slides > .slide > .video-content {
  height: 100%;
}
* + html .blueimp-gallery > .slides > .slide > .video-content > .video-play {
  left: 50%;
  margin-left: -64px;
}

.blueimp-gallery > .slides > .slide > .video-content > .video-play:hover {
  opacity: 1;
}
    </style>