 <!--==========================
      testimonials Us Section
    ============================-->
    <section id="pricing">
      <div class="container">
        <div class="row testimonial-container">
          <div class="col-lg-12 content">
            <h2 class="title">Pricing</h2>
            <div class="border"></div>
            <br>

            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="card">
                  <div class="card-body">
                    <p class="card-text">We appreciate the fact that every business and every situation is unique. We want to offer custom  made affordable  pricing tailored as per your requirements.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
              <div class="card">
                  <div class="card-body">
                    <p class="card-text">At the same time, we guarantee you that our solutions will be of best quality and highly affordable at the same time.  Please contact us for more information</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- #about -->