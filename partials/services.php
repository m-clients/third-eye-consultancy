 <!--==========================
      testimonials Us Section
    ============================-->
    <section id="services">
      <div class="container">
        <div class="row testimonial-container">
          <div class="col-lg-10 offset-lg-1 content">
            <div class="content-header">
              <h2 class="title">Services</h2>
              <div class="border"></div>
              <br>
            </div>
            

             
              <!-- Nav tabs -->
              <ul class="nav nav-tabs tabs nav-pills nav-fill">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#book">Bookkeeping Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#finance">Financial projections & Forcasting</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#dynamic">Dynamic & Interactive Dashboards</a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content" id='my-tab-content'>
                <div class="tab-pane container active" id="book">
                  <p>We provide a wide portfolio of bookkeeping services. Some of them are listed as below:</p>
                  <ul  class="list-group list-group-flush">
                  <li class="list-group-item">Annual bookkeeping for tax season</li>
                  <li class="list-group-item">Quarterly bookkeeping</li>
                  <li class="list-group-item">Monthly bookkeeping</li>
                  <li class="list-group-item">Daily/ Full Charge Bookkeeping</li>
                  <li class="list-group-item">Catchup, bookkeeping for previous years</li>
                  <li class="list-group-item">QBO setup</li>
                  <li class="list-group-item">Wave, Xero, other accounting softwares setup</li>
                  <li class="list-group-item">DIY Consultations &amp; Training</li>
                </ul>
                </div>
                <div class="tab-pane container fade" id="finance">
                  <p>Want to see where your business will be in 5 years from now? Then this is for you:</p>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item">Projected Profit &amp; Loss Account</li>
                    <li class="list-group-item">Projected Balance sheet and Cash flow statement</li>
                    <li class="list-group-item">Key Financial figures such as Break even point, ROI, Product wise profitability etc</li>
                  </ul>
                </div>
                <div class="tab-pane container fade" id="dynamic">
                  <p>Don’t have time to dig into your accoutning software to understand the results? 								
              No Worries, we will prepare dynamic and interactive dashboards with different charts for quick interpretation</p>
                </div>
              </div>
            <!-- //END tab -->

            <div class="content-header">
              <br>
              <p class="sub-title">Accounting for E commerce Sellers</p>
              <p>Are you a seller on Amazon seller central, Shopify, ETSY or any other marketplace?</p>
              <p>Then this one is for you :</p>
            </div>
            <div class="video-container">
              <iframe src="https://www.youtube.com/embed/vgkR7aHPMHU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

          </div>
        </div>
      </div>
    </section><!-- #about -->