var slides = document.getElementsByClassName("mySlides");
var dots = document.getElementsByClassName("dot");
var prev = document.querySelector(".prev-quote");
var next = document.querySelector(".next-quote");

if (!slides.length == 0) {
    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides((slideIndex += n));
    }

    var currentSlide = function(n) {
        showSlides((slideIndex = n));
    };

    function showSlides(n) {
        if (n > slides.length) {
            slideIndex = 1;
        }

        if (n < 1) {
            slideIndex = slides.length;
        }

        for (i = 0; i < slides.length; i++) {
            // slides[i].classList.remove('active');
            $(slides[i]).hide();
        }
        // slides[slideIndex - 1].classList.add('active');
        $(slides[slideIndex - 1]).fadeIn(500);
    }
}

prev.addEventListener("click", () => {
    plusSlides(-1);
});

next.addEventListener("click", () => {
    plusSlides(1);
});