/**
 * Exit entent
 * This file depends on stick-to-me.js lib
 */
$(document).ready(function() {
    $.stickToMe({
        layer: '#exitEntendModal',
        cookie: true,
        maxamount: 1,
        bgclickclose: false,
        cookieExpiration: 1 * 24 * 60 * 60 * 1000
    });
});