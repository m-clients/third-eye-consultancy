$(document).ready(function() {
    var accordion = $("#accordion .card-header");
    accordion.map(function(index, item, array) {
        $(item).on('click', function() {
            $(this).toggleClass('active');
            $(this).next().toggleClass('show-content');
        });
    });
});