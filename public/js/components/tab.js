jQuery(document).ready(function($) {
    // Creating tab
    function eventHandler(e) {
        e.preventDefault();
        var targetID = e.target.getAttribute('href');
        var targetTabContent = $("#my-tab-content " + targetID);
        disablingAllTabContents($("#my-tab-content .tab-pane"));
        disablingAllTabContents($(".tabs .nav-link"));
        targetTabContent.addClass('active');
        this.classList.add('active');
    }

    var tabs = $(".tabs .nav-link");
    var ids = [];
    tabs.map(function(index, item, array) {
        ids.push(item.getAttribute('href'));
        item.addEventListener('click', eventHandler);
    });

    function disablingAllTabContents(elements) {
        elements.map(function(index, item) {
            item.classList.remove("fade", "active");
        });
    }
});