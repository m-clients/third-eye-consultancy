const { series, src, dest, watch } = require('gulp');
const minifyCss = require('gulp-clean-css');
const rename = require("gulp-rename");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");


const appCss = () => {
    return src('./public/css/style.css')
        .pipe(minifyCss())
        .pipe(rename('app.css'))
        .pipe(dest('./public/build/css/'));
}

const appJS = () => {
    return src('./public/js/components/*.js')
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(dest('./public/build/js/'));
}

exports.appCss = appCss;
exports.appJS = appJS;
exports.default = series(appCss, appJS);